// add filter and action from wp hooks.
const { addFilter, addAction } = wp.hooks;

const ItemTypes = {
	CARD: 'card',
	START_ELEMENTS: 'START_ELEMENTS',
	CONTENT_ELEMENTS: 'CONTENT_ELEMENTS',
	RESULT_ELEMENTS: 'RESULT_ELEMENTS'
}

addFilter('contentElements', 'data', contentElements);

function contentElements(data) {
	return [
		{
			name: 'Text',
			componentName: 'TextElement',
			itemType: ItemTypes.CONTENT_ELEMENTS,
			proVersionQuestionType: true,
		},
		{
			name: 'Image Question',
			componentName: 'ImageQuestion',
			itemType: ItemTypes.CONTENT_ELEMENTS,
			proVersionQuestionType: true,
		}
	];
}