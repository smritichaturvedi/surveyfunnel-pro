const { addFilter } = wp.hooks;
import React from 'react';

addFilter( 'choicesState', 'ContentElements', choicesState );

function choicesState( data, type ) {
	if ( type === 'scoring' ) {
		return {
			score: 0,
		}
	}
	else {
		return {}
	}
}

addFilter( 'answersLeftBoxes', 'ContentElements', answersLeftBoxes );

function answersLeftBoxes(data, type, answer, handleAnswerChange, idx) {

	if ( type === 'scoring' ) {
		return <input
			type="number"
			placeholder={`Score #${
				idx + 1
			}`}
			value={answer.score}
			onChange={handleAnswerChange(
				idx,
				'score'
			)}
		/>
	}

	return '';
}

let exportFunctionsForTesting = { choicesState, answersLeftBoxes };
export default exportFunctionsForTesting;