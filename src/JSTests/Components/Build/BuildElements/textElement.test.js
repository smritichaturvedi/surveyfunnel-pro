import React from 'react';
import { shallow } from 'enzyme';
import TextElement from '../../../../Components/Build/BuildElements/TextElement';

function ModalContentRight() {
	return <></>
}

function CloseModal() {
	return <></>
}

function saveToList() {

}

describe('testing text element react component', () => {
	it( 'should render text element component', () => {
		const component = shallow( <TextElement designCon={{}} currentElement={{
			componentName: 'TextElement',
			currentlySaved: true,
			title: '',
			button: '',
			description: '',
		}} ModalContentRight={ModalContentRight} saveToList={saveToList} CloseModal={CloseModal}  /> );

		const form = component.find('input');
		const button = component.find('button');
		button.props().onClick({ target: {
			name: 'save'
		}});

		expect(component.state('error')).toBe('Please add title before saving.');

		form.props().onChange({ target: {
			name: 'title',
			value: 'myValue'
		}});

		button.props().onClick({ target: {
			name: 'save'
		}});

		expect(component.state('title')).toBe('myValue');
	} )
})